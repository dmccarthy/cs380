using UnityEngine;
using System.Collections;

public class DoneEnemyAI : MonoBehaviour
{
    public float patrolSpeed = 2f;							// The nav mesh agent's speed when patrolling.
    public float chaseSpeed = 5f;							// The nav mesh agent's speed when chasing.
    public float chaseWaitTime = 5f;						// The amount of time to wait when the last sighting is reached.
    public float patrolWaitTime = 1f;						// The amount of time to wait when the patrol way point is reached.
    public Transform[] patrolWayPoints;						// An array of transforms for the patrol route.
    public bool useBehaviorTree = false;
    public bool alerted = false;
    

    private DoneEnemySight enemySight;						// Reference to the EnemySight script.
    private NavMeshAgent nav;								// Reference to the nav mesh agent.
    private Transform player;								// Reference to the player's transform.
    private DonePlayerHealth playerHealth;					// Reference to the PlayerHealth script.
    private DoneLastPlayerSighting lastPlayerSighting;		// Reference to the last global sighting of the player.
    private float chaseTimer;								// A timer for the chaseWaitTime.
    public float patrolTimer;								// A timer for the patrolWaitTime.
    private int wayPointIndex;								// A counter for the way point array.
    private BehaviorTree enemy_btree;
    public bool gunOut;
    private float resetChase;
    public string debug_btreeText;
    public float debugStringTimer;
    public GUIText debugstring_Writer;
    
    
    void Awake ()
    {
        // Setting up the references.
        enemySight = GetComponent<DoneEnemySight>();
        nav = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag(DoneTags.player).transform;
        playerHealth = player.GetComponent<DonePlayerHealth>();
        lastPlayerSighting = GameObject.FindGameObjectWithTag(DoneTags.gameController).GetComponent<DoneLastPlayerSighting>();
        debugstring_Writer = GameObject.Find("gui_textTEST").GetComponent<GUIText>();
        gunOut = false;
        alerted = false;
        enemy_btree = new BehaviorTree(gameObject);
        resetChase = 0;
        debugStringTimer = 0;
        CreateEnemyBTree();
    }


    private void CreateEnemyBTree()
    {
        TreeBehavior checkNearB = new PlayerNear_Behavior();
        TreeBehavior playerVisibleB = new PlayerInSight_Behavior();
        TreeBehavior gunOutB = new GunIsOut_Behavior();
        TreeBehavior shootB = new Shoot_Behavior();
        TreeBehavior chaseB = new ChaseBehavior();
        TreeBehavior patrolB = new PatrolBehavior();
        TreeBehavior alertedB = new Alerted_Behavior();
        TreeBehavior takeOutGunB = new TakeOutGun_Behavior();

        //Crappy initialization of owner
        checkNearB.m_owner = gameObject;
        playerVisibleB.m_owner = gameObject;
        gunOutB.m_owner = gameObject;
        shootB.m_owner = gameObject;
        chaseB.m_owner = gameObject;
        patrolB.m_owner = gameObject;
        takeOutGunB.m_owner = gameObject;
        alertedB.m_owner = gameObject;

        //Right Tree
        Sequence patrolTree = new Sequence();
        patrolTree.AddChild(new Inverter(playerVisibleB));
        patrolTree.AddChild(patrolB);

        //Left Tree
        Sequence gunOutShoot = new Sequence();
        gunOutShoot.AddChild(gunOutB);
        gunOutShoot.AddChild(shootB);

        Sequence nearCheck = new Sequence();
        nearCheck.AddChild(checkNearB);
        nearCheck.AddChild(takeOutGunB);

        Selector nearFar = new Selector();
        nearFar.AddChild(nearCheck);
        nearFar.AddChild(chaseB);

        Selector pvisGunOut = new Selector();
        pvisGunOut.AddChild(new Inverter(playerVisibleB));
        pvisGunOut.AddChild(gunOutShoot);
        pvisGunOut.AddChild(nearFar);

        Selector pvOrchase = new Selector();
        pvOrchase.AddChild(pvisGunOut);
        pvOrchase.AddChild(chaseB);

        Sequence alertcheckSequence = new Sequence();
        alertcheckSequence.AddChild(alertedB);
        alertcheckSequence.AddChild(pvOrchase);

        Selector alertedOrPatrol = new Selector();
        alertedOrPatrol.AddChild(alertcheckSequence);
        alertedOrPatrol.AddChild(patrolTree);

        enemy_btree.m_owner = gameObject;
        enemy_btree.m_root = alertedOrPatrol;
    }

    void Update ()
    {
        if (useBehaviorTree) 
        {
            if (alerted)
            {
                resetChase += Time.deltaTime;
                if (resetChase > 3.0f)
                {
                    resetChase = 0;
                    alerted = false;
                    enemySight.personalLastSighting = lastPlayerSighting.resetPosition;
                    lastPlayerSighting.position = lastPlayerSighting.resetPosition;
                }
            }
            else
              resetChase = 0;

            debugStringTimer += Time.deltaTime;
            if (debugStringTimer > 1.0f)
            {
                debugStringTimer = 0;
                debug_btreeText = "";
                enemy_btree.Update();
                Debug.Log(debugstring_Writer);
                debugstring_Writer.text = debug_btreeText;
            }
            else
            {
                debug_btreeText = "";
                enemy_btree.Update();
            }

            


        }
        else
        {
            if(enemySight.playerInSight && playerHealth.health > 0f)
                // ... shoot.
                Shooting();
            
            // If the player has been sighted and isn't dead...
            else if(enemySight.personalLastSighting != lastPlayerSighting.resetPosition && playerHealth.health > 0f)
                // ... chase.
                Chasing();
            
            // Otherwise...
            else
                // ... patrol.
                Patrolling();
        }
        // If the player is in sight and is alive...
    }
    
    
    void Shooting ()
    {
        // Stop the enemy where it is.
        nav.Stop();
    }
    
    
    void Chasing ()
    {
        // Create a vector from the enemy to the last sighting of the player.
        Vector3 sightingDeltaPos = enemySight.personalLastSighting - transform.position;
        
        // If the the last personal sighting of the player is not close...
        if(sightingDeltaPos.sqrMagnitude > 4f)
            // ... set the destination for the NavMeshAgent to the last personal sighting of the player.
            nav.destination = enemySight.personalLastSighting;
        
        // Set the appropriate speed for the NavMeshAgent.
        nav.speed = chaseSpeed;
        
        // If near the last personal sighting...
        if(nav.remainingDistance < nav.stoppingDistance)
        {
            // ... increment the timer.
            chaseTimer += Time.deltaTime;
            
            // If the timer exceeds the wait time...
            if(chaseTimer >= chaseWaitTime)
            {
                // ... reset last global sighting, the last personal sighting and the timer.
                lastPlayerSighting.position = lastPlayerSighting.resetPosition;
                enemySight.personalLastSighting = lastPlayerSighting.resetPosition;
                chaseTimer = 0f;
            }
        }
        else
            // If not near the last sighting personal sighting of the player, reset the timer.
            chaseTimer = 0f;
    }

    
    void Patrolling ()
    {
        // Set an appropriate speed for the NavMeshAgent.
        nav.speed = patrolSpeed;
        
        // If near the next waypoint or there is no destination...
        if(nav.destination == lastPlayerSighting.resetPosition || nav.remainingDistance < nav.stoppingDistance)
        {
            // ... increment the timer.
            patrolTimer += Time.deltaTime;
            
            // If the timer exceeds the wait time...
            if(patrolTimer >= patrolWaitTime)
            {
                // ... increment the wayPointIndex.
                if(wayPointIndex == patrolWayPoints.Length - 1)
                    wayPointIndex = 0;
                else
                    wayPointIndex++;
                
                // Reset the timer.
                patrolTimer = 0;
            }
        }
        else
            // If not near a destination, reset the timer.
            patrolTimer = 0;
        
        // Set the destination to the patrolWayPoint.
        nav.destination = patrolWayPoints[wayPointIndex].position;
    }


}

public class StopBehavior : TreeBehavior
{   
    private NavMeshAgent nav;								// Reference to the nav mesh agent.
    
    public override void OnInitialize()
    {
        nav = GetOwner().GetComponent<NavMeshAgent>();
    }

    public override Status Update()
    {
        // Stop the enemy where it is.
        nav.Stop();
        Debug.Log("StopBehavior");

        return Status.SUCCESS;
    }

    //public override void OnExit(Status status)
   // {
   //
   // }
};

public class TakeOutGun_Behavior : TreeBehavior
{

    public override void OnInitialize()
    {
    }

    public override Status Update()
    {
        Debug.Log("TakeOutGun_Behavior");
        // Stop the enemy where it is.
        m_owner.GetComponent<DoneEnemyAI>().gunOut = true;

        return Status.SUCCESS;
    }

    //public override void OnExit(Status status)
    //{
    //
    //}
};
 
public class ChaseBehavior : TreeBehavior
{
    public float patrolSpeed = 2f;							// The nav mesh agent's speed when patrolling.
    public float gunOutSpeed = 1f;
    public float chaseSpeed = 5f;							// The nav mesh agent's speed when chasing.
    public float chaseWaitTime = 5f;						// The amount of time to wait when the last sighting is reached.
    public float patrolWaitTime = 1f;						// The amount of time to wait when the patrol way point is reached.
    public Transform[] patrolWayPoints;						// An array of transforms for the patrol route.
    public bool useBehaviorTree = false;
    
    private DoneEnemySight enemySight;						// Reference to the EnemySight script.
    private NavMeshAgent nav;								// Reference to the nav mesh agent.
    private Transform player;								// Reference to the player's transform.
    private DonePlayerHealth playerHealth;					// Reference to the PlayerHealth script.
    private DoneLastPlayerSighting lastPlayerSighting;		// Reference to the last global sighting of the player.
    private float chaseTimer;								// A timer for the chaseWaitTime.
    private float patrolTimer;								// A timer for the patrolWaitTime.
    private int wayPointIndex;								// A counter for the way point array.
    private BehaviorTree enemy_btree;
    private Transform transform;

    public override void OnInitialize()
    {
        enemySight = GetOwner().GetComponent<DoneEnemySight>();
        nav = GetOwner().GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag(DoneTags.player).transform;
        playerHealth = player.GetComponent<DonePlayerHealth>();
        lastPlayerSighting = GameObject.FindGameObjectWithTag(DoneTags.gameController).GetComponent<DoneLastPlayerSighting>();
        transform = GetOwner().transform;
    }

    public override Status Update()
    {
        //Debug.Log("Chasing");

        // Stop the enemy where it is.
        // Create a vector from the enemy to the last sighting of the player.
        Vector3 sightingDeltaPos = enemySight.personalLastSighting - transform.position;

        // If the the last personal sighting of the player is not close...
        if (sightingDeltaPos.sqrMagnitude > 4f)
            // ... set the destination for the NavMeshAgent to the last personal sighting of the player.
            nav.destination = enemySight.personalLastSighting;

        // Set the appropriate speed for the NavMeshAgent.
        nav.speed = chaseSpeed;

        if (GetOwner().GetComponent<DoneEnemyAI>().gunOut)
            nav.speed = gunOutSpeed;

        // If near the last personal sighting...
        if (nav.remainingDistance < nav.stoppingDistance)
        {
            // ... increment the timer.
            chaseTimer += Time.deltaTime;

            // If the timer exceeds the wait time...
            if (chaseTimer >= chaseWaitTime)
            {
                // ... reset last global sighting, the last personal sighting and the timer.
                lastPlayerSighting.position = lastPlayerSighting.resetPosition;
                enemySight.personalLastSighting = lastPlayerSighting.resetPosition;
                chaseTimer = 0f;
                DoneEnemyAI ai = GetOwner().GetComponent<DoneEnemyAI>();
                ai.alerted = false;
                return Status.FAILURE;
            }
            return Status.SUCCESS;
        }
        else
            // If not near the last sighting personal sighting of the player, reset the timer.
            chaseTimer = 0f;

        return Status.SUCCESS;
    }

    //public override void OnExit(Status status)
    //{
    //}
};

public class PatrolBehavior : TreeBehavior
{
    public float patrolSpeed = 2f;							// The nav mesh agent's speed when patrolling.
    public float chaseSpeed = 5f;							// The nav mesh agent's speed when chasing.
    public float chaseWaitTime = 5f;						// The amount of time to wait when the last sighting is reached.
    public float patrolWaitTime = 1f;						// The amount of time to wait when the patrol way point is reached.
    public Transform[] patrolWayPoints;						// An array of transforms for the patrol route.
    public bool useBehaviorTree = false;

    private DoneEnemySight enemySight;						// Reference to the EnemySight script.
    private NavMeshAgent nav;								// Reference to the nav mesh agent.
    private Transform player;								// Reference to the player's transform.
    private DonePlayerHealth playerHealth;					// Reference to the PlayerHealth script.
    private DoneLastPlayerSighting lastPlayerSighting;		// Reference to the last global sighting of the player.
    private float patrolTimer;								// A timer for the patrolWaitTime.
    private int wayPointIndex;								// A counter for the way point array.
    private BehaviorTree enemy_btree;
    private bool gunOut;

    public override void OnInitialize()
    {
        nav = GetOwner().GetComponent<NavMeshAgent>();
        enemySight = GetOwner().GetComponent<DoneEnemySight>();
        player = GameObject.FindGameObjectWithTag(DoneTags.player).transform;
        playerHealth = player.GetComponent<DonePlayerHealth>();
        lastPlayerSighting = GameObject.FindGameObjectWithTag(DoneTags.gameController).GetComponent<DoneLastPlayerSighting>();
        gunOut = false;
        patrolWayPoints = GetOwner().GetComponent<DoneEnemyAI>().patrolWayPoints;
        //patrolTimer = GetOwner().GetComponent<DoneEnemyAI>().patrolTimer;
    }

    public override Status Update()
    {
        // Set an appropriate speed for the NavMeshAgent.
        nav.speed = patrolSpeed;

        // If near the next waypoint or there is no destination...
        if (nav.destination == lastPlayerSighting.resetPosition || nav.remainingDistance < nav.stoppingDistance)
        {
            // ... increment the timer.
            patrolTimer += Time.deltaTime;

            // If the timer exceeds the wait time...
            if (patrolTimer >= patrolWaitTime)
            {
                // ... increment the wayPointIndex.
                if (wayPointIndex == patrolWayPoints.Length - 1)
                    wayPointIndex = 0;
                else
                    wayPointIndex++;

                // Reset the timer.
                patrolTimer = 0;
                return Status.SUCCESS;
            }
        }
        else             // If not near a destination, reset the timer.
            patrolTimer = 0;

        // Set the destination to the patrolWayPoint.
        nav.destination = patrolWayPoints[wayPointIndex].position;
        return Status.SUCCESS;
    }

    //public override void OnExit(Status status)
    //{
    //}

};
