﻿using UnityEngine;

//////////////////////////////////////////////////////////////////
//                   Decorator Base Class                       //
//////////////////////////////////////////////////////////////////

class Decorator : TreeBehavior
{
  public Decorator(TreeBehavior child)
  {
    m_child = child;
  }

  public void SetChild(TreeBehavior child)
  {
    m_child = child;
  }

  // Member Variables
  protected TreeBehavior m_child;
}

//////////////////////////////////////////////////////////////////
//                          Repeat                              //
//             Runs child a set number of times                 //
//////////////////////////////////////////////////////////////////
class Repeat : Decorator
{
  public Repeat(TreeBehavior child, int count = 1)
    : base(child)
  {
    // Count cannot be lower than 1
    count = count > 1 ? count : 1;

    m_iteration = 0;
    m_repeatCount = count;
  }

  public override void OnInitialize()
  {
    m_iteration = 0;
  }

  public override Status Update()
  {
    while(m_iteration < m_repeatCount)
    {
      Status result = m_child.Run();

      if(result == Status.FAILURE)
        return Status.FAILURE;

      ++m_iteration;
      if(m_iteration == m_repeatCount)
        return Status.SUCCESS;
    }

    // Should not reach here
    return Status.FAILURE;
  }

  
  // Member Variables
  private int m_repeatCount;
  private int m_iteration;
}

//////////////////////////////////////////////////////////////////
//                     Repeat Until Fail                        //
//                 Runs child until it fails                    //
//////////////////////////////////////////////////////////////////
class RepeatUntilFail : Decorator
{
  public RepeatUntilFail(TreeBehavior child)
    : base(child)
  {
  }

  public override Status Update()
  {
    while(true)
    {
      Status result = m_child.Run();

      if(result == Status.FAILURE)
        return result;
    }
  }
}

//////////////////////////////////////////////////////////////////
//                         Inverter                             //
//           Inverts success/fail result of child               //
//////////////////////////////////////////////////////////////////
class Inverter : Decorator
{
  public Inverter(TreeBehavior child)
    : base(child)
  {
  }

  public override Status Update()
  {
    Status result = m_child.Run();

    if(result == Status.SUCCESS)
      return Status.FAILURE;
    if(result == Status.FAILURE)
      return Status.SUCCESS;
    else
      return result;
  }
}