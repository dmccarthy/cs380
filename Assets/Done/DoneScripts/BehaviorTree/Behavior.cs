﻿using UnityEngine;
using System.Collections;

public class TreeBehavior
{
  public TreeBehavior()
  {
    m_status = Status.INVALID;
    m_owner = null;
  }
  
  public Status Run()
  {
    // Initialize if not running
    if(m_status != Status.RUNNING)
      OnInitialize();

    m_status = Update();

    // If it is not running anymore, it has either
    // succeeded, failed, or suspended
    if(m_status != Status.RUNNING)
      OnExit(m_status);

    return m_status;
  }

  public GameObject GetOwner()
  {
      return m_owner;
  }

  public Status GetStatus()
  {
    return m_status;
  }

  // Virtual functions
  public virtual void OnInitialize()
  {
  }

  public virtual Status Update()
  {
      return Status.SUCCESS;
  }

  public virtual void OnExit(Status status)
  {
      if(m_owner)
        if (status.Equals(Status.SUCCESS))
        {
           // Debug.Log(m_owner.GetComponent<DoneEnemyAI>().debug_btreeText.Length);
            m_owner.GetComponent<DoneEnemyAI>().debug_btreeText += "-> " + this.ToString();//.Insert(m_owner.GetComponent<DoneEnemyAI>().debug_btreeText.Length, );
        }

  }


  // Member Variables
  private Status m_status;
  public GameObject m_owner;
}
