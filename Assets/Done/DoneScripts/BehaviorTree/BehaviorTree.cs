﻿using UnityEngine;

public enum Status
{
  INVALID,
  RUNNING,
  SUCCESS,
  FAILURE,
}

public class BehaviorTree 
{
  public BehaviorTree(GameObject owner)
  {
      m_owner = owner;
  }

  public BehaviorTree(TreeBehavior root)
  {
    m_root = root;
  }

  public Status Update()
  {
    if(m_root != null)
    {
      return m_root.Run();
    }
    else
      return Status.FAILURE;
  }


  // Member Variables
  public TreeBehavior m_root;
  public GameObject m_owner;
}
