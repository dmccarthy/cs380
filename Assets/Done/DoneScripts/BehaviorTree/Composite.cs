﻿using UnityEngine;
using System.Collections.Generic;

//////////////////////////////////////////////////////////////////
//                   Composite Base Class                       //
//////////////////////////////////////////////////////////////////

class Composite : TreeBehavior
{
  public Composite()
  {
    m_children = new List<TreeBehavior> ();
  }

  public void AddChild(TreeBehavior child)
  {
    m_children.Add(child);
  }

  public bool RemoveChild(TreeBehavior child)
  {
    return m_children.Remove(child);
  }

  public void ClearChildren()
  {
    m_children.Clear();
  }

  public TreeBehavior this[int i]
  {
    get { return m_children[i]; }
    set { m_children[i] = value; }
  }


  // Member Variables
  protected List<TreeBehavior> m_children;
}

//////////////////////////////////////////////////////////////////
//                          Selector                            //
//          Runs children in order until one succeeds           //
//////////////////////////////////////////////////////////////////

class Selector : Composite
{
  public Selector()
  {
    m_currIndex = 0;
  }

  public override void OnInitialize()
  {
    m_currIndex = 0;
  }

  public override Status Update()
  {
    // Runs children in order until one succeeds
    while(true)
    {
      Status result = m_children[m_currIndex].Run();

      if(result != Status.FAILURE)
        return result;

      ++m_currIndex;
      if(m_currIndex == m_children.Count)
        return Status.FAILURE;
    }
  }


  // Member Variables
  int m_currIndex;
}

//////////////////////////////////////////////////////////////////
//                          Sequence                            //
//      Runs all children in sequence until failure or end      //
//////////////////////////////////////////////////////////////////

class Sequence : Composite
{
  public Sequence()
  {
    m_currIndex = 0;
  }

  public override void OnInitialize()
  {
    m_currIndex = 0;
  }

  public override Status Update() 
  {
    // Runs all children in sequence until failure or end
    while(true)
    {
      Status result = m_children[m_currIndex].Run();

      if(result != Status.SUCCESS)
        return result;

      ++m_currIndex;
      if(m_currIndex == m_children.Count)
        return Status.SUCCESS;
    }
  }

  public override void OnExit(Status status)
  {
  }


  // Member Variables
  protected int m_currIndex;
}

//////////////////////////////////////////////////////////////////
//                          Parallel                            //
//      Runs all children. Failure or success depending on      //
//             whether one/all succeeds or fails                //
//////////////////////////////////////////////////////////////////
class Parallel : Composite
{
  public enum Condition
  {
    REQUIRE_ONE,
    REQUIRE_ALL
  }

  Parallel(Condition forSuccess, Condition forFailure)
  {
    m_forSuccess = forSuccess;
    m_forFailure = forFailure;
  }

  public override Status Update()
  {
    int successCount = 0;
    int failureCount = 0;

    foreach(TreeBehavior b in m_children)
    {
      Status status = b.GetStatus();

      if(!(status == Status.SUCCESS || status == Status.RUNNING))
        status = b.Run();


      if(status == Status.SUCCESS)
        successCount++;

      if(status == Status.FAILURE)
        failureCount++;
    }

    // Require One condition biases towards success
    if(m_forSuccess == Condition.REQUIRE_ONE && successCount > 0)
      return Status.SUCCESS;

    if(m_forFailure == Condition.REQUIRE_ONE && failureCount > 0)
      return Status.FAILURE;

    

    if(m_forFailure == Condition.REQUIRE_ALL && failureCount == m_children.Count)
      return Status.FAILURE;

    if(m_forSuccess == Condition.REQUIRE_ALL && successCount == m_children.Count)
      return Status.SUCCESS;

    return Status.RUNNING;
  }

  
  // Member Variables
  protected Condition m_forSuccess;
  protected Condition m_forFailure;
}